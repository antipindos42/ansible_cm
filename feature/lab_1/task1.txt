invenotory/hosts
[localhost]
serv-localhost ansible_connection=local ansible_host=localhost

[test_staging_servers]
linux1 ansible_host=192.168.1.14


[test_prod_servers]
linux2 ansible_host=192.168.1.15



[redhat_servers]


[all_servers]
debian_servers
redhat_servers

inventory/group_vars/all_servers.yml
user:
  login: admin
  password: 12345


roles/deploy_lab_1/tasks/main.yml

---
- name: Ctreate User with password and group
  block:
    - name: Create group
      group:
        name: "{{ user.group }}"

    - name: Create User
      user:
        name: "{{ user.login }}"
        passwword:  "{{ user.password | password_hash('sha512') }}"
        shell: /bin/bash
        group: "{{ user.group }}"
  rescuse:
    - name: Rollback changes if need
      command: date

- name: Install packages to servers
  package:
    name:
      - iftop
      - htop
      - vim
      - tcpdump
      - unzip
      - telnet
      - etc
    state: latest

- name: create directory and load  files gitlab  repository to servers
  block:
    - name: create directory
      file: /home/my_repo_gitlab/version1 state=directory
    - name: load file
      get_url:
      url: https://gitlab.com/-/ide/project/antipindos42/ansible_cm/edit/develop/-/feature/lab_1/task1.txt
      dest: "{{ destin_folder }}"    #vars: destin_folder: /home/my_repo_gitlab/version1
      mode: '0555'

- name: Install and start  Nginx Web Server
  block: Install and start Nginx Web Server on Debian Family
    - name: Install Nginx Web Server
      apt:
      name=nginx
      state=latest
    - name: Start Nginx Web Server
      servces:
      name=nginx state=started enabled=yes
  when:
    ansible_os_family == "Debian"
  notify:
    - Restart nginx Debian


handlers/main.ymal
- name: Restarted nginx Debian
  service:
    name: nginx
    state: restarted
  when: ansible_os_family == "Debian"

roles/deploy_lab_1/defaults/main.yml

---
user:
  login: app
  password: app
  group: app
vars:
  destin_folder: /home/my_repo_gitlab/version1


playbook_lab_1.yml

---
- name: preparing and configuring the server, installing nginx
  hosts: all_servers
  become: yes

  roles:
    - { role: deploy_lab_1, when: ansible_system == 'Linux' }


